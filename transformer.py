import sys
import pandas as pd
from rdflib import Graph, Literal, Namespace, RDF, URIRef, XSD

def transform_dataset(dataset_path):
    print("Loading dataset from:", dataset_path)
    
    # Load dataset
    data_frame = pd.read_csv(dataset_path, index_col=0, parse_dates=True)
    
    # Print column names for debugging
    print("Columns in CSV:", data_frame.columns)

    # Strip whitespace from column names
    data_frame.columns = data_frame.columns.str.strip()

    # Check if the required columns are present
    required_columns = ['cet_cest_timestamp', 'interpolated']
    for col in required_columns:
        if col not in data_frame.columns:
            print(f"Error: Column '{col}' not found in the CSV file.")
            return

    data_frame = data_frame.drop(required_columns, axis=1)

    # Define namespaces
    SAREF = Namespace("https://saref.etsi.org/core/v3.1.1/saref.ttl")
    EX = Namespace("http://example.org/Haris")

    # Create RDF graph and bind namespaces
    rdf_graph = Graph()
    rdf_graph.bind("saref", SAREF)
    rdf_graph.bind("ex", EX)

    unit_kwh = URIRef(EX['Units/kWh'])

    # Process each row in the dataset
    for timestamp, row in data_frame.iterrows():
        timestamp_uri = URIRef(EX[f'timestamps/{timestamp.isoformat()}'])

        for column_name, value in row.items():
            if pd.notna(value):
                building, device = column_name.rsplit('_', 2)[0], '_'.join(column_name.rsplit('_', 2)[1:])
                
                device_uri = URIRef(EX[f'Devices/{building}/{device}'])
                building_uri = URIRef(EX[f'Buildings/{building}'])
                measurement_uri = URIRef(EX[f'Measurements/{building}/{device}/{timestamp.isoformat()}'])

                # Add triples to the RDF graph
                rdf_graph.add((measurement_uri, RDF.type, SAREF.Measurement))
                rdf_graph.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp.isoformat(), datatype=XSD.dateTime)))
                rdf_graph.add((measurement_uri, SAREF.isMeasuredIn, unit_kwh))
                rdf_graph.add((measurement_uri, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))
                rdf_graph.add((measurement_uri, EX['is_measurement_Of'], device_uri))

                rdf_graph.add((device_uri, RDF.type, SAREF.Device))
                rdf_graph.add((device_uri, EX['has_measurement'], measurement_uri))
                rdf_graph.add((device_uri, SAREF.isContainedIn, building_uri))

                rdf_graph.add((building_uri, RDF.type, SAREF.Building))
                rdf_graph.add((building_uri, SAREF.Contains, device_uri))

    print("Finished processing. Serializing RDF data...")
    # Serialize and save the RDF graph
    rdf_graph.serialize(format='turtle', destination="rdf_data.ttl")
    print("RDF data saved to rdf_data.ttl")

def main():
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        sys.exit(1)
    dataset_path = sys.argv[1]
    transform_dataset(dataset_path)

if __name__ == "__main__":
    main()
